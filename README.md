# Local Prometheus Metrics

This is an app powered by [Docker Compose](https://docs.docker.com/compose/) which makes it easy to generate Prometheus data locally, without the need to deploy to an external service such as a Kubernetes cluster.

##  Requirements
- Docker & Docker Compose: https://docs.docker.com/compose/install/

## Instructions

1. Run `docker-compose up`
2. That's it!

The following services will be running:

| Port  | Service  | URL |
|---|---|---|
| 8080 | Nginx Webapp  | http://localhost:8080  |
| 8081  | Nginx metrics exporter  | http://localhost:8081   |
| 9090  | Prometheus console  | http://localhost:9090  |

### Generating Responses

Just visit http://localhost:8080 to generate `200` responses.

You can also use the following:

- http://localhost:8080/400 - `400` response.
- http://localhost:8080/500 - `500` response.
- http://localhost:8080/flakey - A random `200`, `400` or `500` response.


### Connecting to GitLab metrics

1. Create a GitLab project
1. Create a `gitlab-ci.yml` with a basic deployment script. [`gitlab-ci.yml.example`](gitlab-ci.yml.example) has a minimal deployment script which will create an environment named `production` & create a deployment.
1. Once you've ran the CI and have an environment, connect your Prometheus instance to GitLab using [these instructions](https://docs.gitlab.com/ee///////user/project/integrations/prometheus.html#manual-configuration-of-prometheus) (you'll have to use http://localhost:9090 as the Prometheus API URL).
1. Make sure that Admin -> Settings -> Network -> Outbound Requests -> Allow requests to the local network from hooks and services is enabled. 

If all goes well, you should start to see metrics on the Dashboard under the **Response metrics (NGINX)** group.

If you named your environment anything other than `production`, you will need to change the environment label in `prometheus.yml`.

## Limitations

- Only **Response metrics (NGINX)** metrics are supported thus far - other [GitLab metrics that require Kubernetes](https://gitlab.com/gitlab-org/gitlab/-/blob/0d4bfea01bbaae52b0f89a22fe75875c9193d6bb/config/prometheus/common_metrics.yml#L1-188) are not supported yet.
- The scraping of metrics seem to generate request metrics. It would maybe be nice if it didn't.
