# Run with: rackup -s thin
# then browse to http://localhost:9292
# Or with: thin start -R config.ru
# then browse to http://localhost:3000
require 'thin'

app = proc do |env|
  req = Rack::Request.new(env)
  case req.path_info
  when /flakey/
    [response_200, response_400, response_500].sample
  when /400/
    response_400
  when /500/
    response_500
  else
    response_200
  end
end

def response_200
  [200, { 'Content-Type' => 'text/html' }, 'Hello world!']
end

def response_400
  [400, { 'Content-Type' => 'text/html' }, 'Bad Request!']
end

def response_500
  [500, { 'Content-Type' => 'text/html' }, 'Error!']
end

run app
